const express = require('express');
const bodyParser = require('body-parser');
const path = require("path");
const fs = require('fs');

// initialize
const app = express();

app.set('json spaces', 2);

app.use(express.static(path.join(__dirname, 'models')));
app.use(express.static(path.join(__dirname, 'views')));
app.use(express.static(path.join(__dirname, 'controllers')));
app.use(express.static(path.join(__dirname, 'services')));

app.set('view engine', 'ejs');
// start the server
app.listen(process.env.PORT || 3000, () => {
    console.log("listening");
});

// bodyparser
app.use(bodyParser.json());

// add api routes
app.use('/api/v1/', require('./routes/api'));

// views
app.get("/", (req, res) => {
    fs.readFile(path.join(__dirname, "data/data.geojson"), 'utf-8', (err, data) => {
        if(!err) {
            res.render('index', {
                status: "success",
                data
            });
        } else {
            res.json(404, {
                status: "error"
            });
        }
    });
});