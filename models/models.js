const multiplier = 100000;

const xDiff = 2907065;
const zDiff = 4100381;

class ScaledPoint {
    constructor(value, y) {
        this.x = value[0] * multiplier - xDiff;
        this.y = y || 0;
        this.z = value[1] * multiplier - zDiff;
    }
}