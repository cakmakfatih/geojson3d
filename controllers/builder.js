"use strict";

(() => {
    const data = JSON.parse(document.currentScript.getAttribute('data'));

    const options = {
        "Vegetation": {
            renderOrder: 1,
            color: 0x2a7a5f
        },
        "Water": {
            renderOrder: 13,
            color: 0x0277bd
        },
        "Open to Below": {
            renderOrder: 3,
            color: 0xd32f2f
        },
        "Area": {
            renderOrder: 4,
            color:0xaaaaaa
        },
        "Room": {
            color: 0x717171,
            renderOrder: 5
        },
        "Non-Public": {
            color: 0x404040,
            renderOrder: 6
        },
        "Walkway": {
            color: 0xffffff,
            renderOrder: 20
        },
        "Escalator": {
            color: 0x703090,
            renderOrder: 8
        },
        "Elevator": {
            color: 0x434b4d,
            renderOrder: 9
        },
        "Stairs": {
            color: 0x663300,
            renderOrder: 10
        },
        "Restroom (Male)": {
            color: 0x128701,
            renderOrder: 11
        },
        "Restroom": {
            color: 0x459a34,
            renderOrder: 11
        },
        "Restroom (Female)": {
            color: 0x78c6b6,
            renderOrder: 11
        },
        "Shopping Center": {
            color: 0x4caf50,
            renderOrder: -1
        }
    };
    
    const scene = new THREE.Scene();
    const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

    const renderer = new THREE.WebGLRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(renderer.domElement);

    window.addEventListener('resize', () => {
        renderer.setSize(window.innerWidth, window.innerHeight);
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
    });

    const controls = new THREE.OrbitControls(camera, renderer.domElement);
    
    const addMarker = (pos) => {
        let geometry = new THREE.SphereGeometry(1, 8, 8);
        let material = new THREE.MeshBasicMaterial({color: 0xf44336});
        let marker = new THREE.Mesh( geometry, material );
        marker.position.x = pos.x;
        marker.position.y = pos.y + 1;
        marker.position.z = pos.z;

        marker.renderOrder = 15;
        material.depthTest = false;

        scene.add(marker);
    };
    
    const addPoint = (f) => {
        let pos = new ScaledPoint(f.geometry.coordinates);

        let sprite = new THREE.TextSprite({
            textSize: 3,
            texture: {
                text: f.properties.CATEGORY,
                fontFamily: 'Arial, Helvetica, sans-serif',
            },
            material: {color: 0xffffff},
        });

        sprite.position.x = pos.x;
        sprite.position.y = pos.y + 3;
        sprite.position.z = pos.z;

        scene.add(sprite);

        addMarker(pos);
    };
    const path = new THREE.Path();

    const addLines = (coords) => {
        coords.slice(1).forEach((i) => {
            i.forEach((j) => {
                let scaledPoint = new ScaledPoint(j);
                path.lineTo(-scaledPoint.z, -scaledPoint.x);
            })
        });
    };

    const addFloor = (f) => {
        let coords = f.geometry.coordinates[0][0];
        let shape = new THREE.Shape();
        let startPos = new ScaledPoint(coords[0]);
        shape.moveTo( -startPos.z, -startPos.x );
        
        coords.slice(1).forEach((i) => {
            let pos = new ScaledPoint(i);
            shape.lineTo( -pos.z, -pos.x );
        });

        let geometry = new THREE.ShapeGeometry( shape );

        let material = new THREE.MeshBasicMaterial({
            color: typeof f.properties.CATEGORY !== 'undefined' ? options[f.properties.CATEGORY].color : 0xcecece
        });

        material.depthTest = false;

        let polygon = new THREE.Mesh(geometry, material);
        material.side = THREE.BackSide;

        if(typeof f.properties.CATEGORY !== 'undefined') {
            polygon.renderOrder = options[f.properties.CATEGORY].renderOrder;
        } else {
            polygon.renderOrder = 0;
        }

        polygon.rotation.x = Math.PI * 3/2;
        polygon.rotation.y = -Math.PI;

        scene.add(polygon);
    };

    const addMultiPolygon = (f) => {
        if(typeof f.properties.HEIGHT !== 'undefined') {
            addWalls(f);
        }
        addFloor(f);
    };

    const addWalls = (f) => {
        let material = new THREE.MeshBasicMaterial({color: 0x313131});
        material.depthTest = false;
        let coordinatesArray = f.geometry.coordinates[0][0];
        let walls = new THREE.Geometry();

        coordinatesArray.forEach((i) => {
            let coordinates = new ScaledPoint(i, f.properties.HEIGHT);
            walls.vertices.push(new THREE.Vector3(coordinates.z, 0, coordinates.x)); 
            walls.vertices.push(new THREE.Vector3(coordinates.z, coordinates.y, coordinates.x));
        });

        let previousVertexIndex = walls.vertices.length - 2;

        for(let i = 0; i < walls.vertices.length; i += 2){
            walls.faces.push(new THREE.Face3(i, i + 1, previousVertexIndex));
            walls.faces.push(new THREE.Face3(i + 1, previousVertexIndex + 1, previousVertexIndex));
            previousVertexIndex = i;
        }

        walls.computeVertexNormals();
        walls.computeFaceNormals();
        material.side = THREE.DoubleSide;
        let items = new THREE.Mesh(walls, material);
        items.renderOrder = 20;
        scene.add(items);
    };
    const up = new THREE.Vector3(0,0,1);
    let position = 0;
    let speed = 0.0003;
    const move = () => {
        position += speed;
        let point = path.getPointAt(position);
        if(point === null) {
            position = 0;
            point = path.getPointAt(position);
        }
        cube.position.x = -point.x;
        cube.position.z = -point.y;
      
        let angle = getAngle(position);
        cube.quaternion.setFromAxisAngle( up, angle );
        
    };

    const getAngle = ( position ) => {
        let tangent = path.getTangent(position).normalize();
    
        let angle = - Math.atan( tangent.x / tangent.y);
        
        return angle;
    };

    const cubeGeometry = new THREE.BoxGeometry(3, 3, 3);
    const cubeMaterial = new THREE.MeshBasicMaterial(
        {
            color: 0xffff00
        }
    );
    
    const cube = new THREE.Mesh(cubeGeometry, cubeMaterial);
    cube.renderOrder = 25;
    cube.position.y = 1.5;
    scene.add(cube);

    // initialize
    const init = () => {
        const lines = [];
        data.features.forEach((f) => {
            camera.position.z = -50;
            camera.position.y = 50;
            camera.position.x = -100;
            camera.lookAt(30, -40, -60);

            switch(f.geometry.type) {
                case "Point":
                    //addPoint(f);
                    break;
                case "LineString":
                    lines.push(f.geometry.coordinates);
                    break;
                case "MultiPolygon":
                    addMultiPolygon(f);
                    break;
                default:
                    break;
            }
        });

        let pathStart = new ScaledPoint(lines[0][0], lines[0][1]);
        path.moveTo(pathStart.z, pathStart.x);
        addLines(lines);

        let points = path.getPoints();
        let geometry = new THREE.BufferGeometry().setFromPoints( points );
        
        let line = new THREE.Line( geometry );
        line.rotation.x = Math.PI / 2;
        line.rotation.z = Math.PI;
        line.renderOrder = 5;
        scene.add( line );
    }
    init();
    const Update = () => {
        move();
    };

    const Render = () => {
        renderer.render(scene, camera);
    };

    const ViewLoop = () => {
        requestAnimationFrame(ViewLoop);
        Update();
        Render();
    };
    
    ViewLoop();
    
})();